# Firmware for my QMK keyboards 
* [QMK configurator](https://config.qmk.fm/)
* [QMKCN for Tofu Kbd6x](http://qmkeyboard.cn/)
* [Minivan configurator](http://qmk.thevankeyboards.com/)
* [QMK Documentation](https://beta.docs.qmk.fm/)

## Keyboard list 

#### [minivan](https://config.qmk.fm/#/thevankeyboards/minivan/LAYOUT_arrow)
* [Layout](/minivan/minivan_layout.PNG)
* limited purple plastic/acrylic case
* hotswap v1 pcb

#### [AMJ40](https://config.qmk.fm/#/amj40/LAYOUT)
* [Layout](/amj40/amj40.PNG)
* Kailh pro purple
* stainless steel plate

#### [Tofu HHKB](http://qmkeyboard.cn/)

* KBD6x hotswap PCB

#### [Duck Raven]()
* [Review here](https://brianlee.blog/2018/11/17/review-duck-raven-and-plateless-mx)
* vintage lubed MX brown 

#### [DZ60]()
* dz60rgb hotswap PCB

