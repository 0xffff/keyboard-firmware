#include QMK_KEYBOARD_H


enum custom_keycodes {
  MC0=SAFE_RANGE , 
  MC1, MC2,
  CIFS,ZSH,BASH,MUL,KLIBO, BRVSIM
};


#define TEST "test"
#define CIFS_TRACE    "sudo sysctl sysvar.sktrace.CIFS_enable = 13 && sysctl sysvar.sktrace.CIFS_AUTH_enable = 13"
#define ZSHRC         "vim $HOME/.zshrc"
#define BASHRC        "vim $HOME/.bashrc"
#define MULTICHANNEL  "cifs options modify -is-multichannel-enabled true"
#define BRVSIM_CMD    "br make vsim final.x86_64.sim"
#define KLIBO_CMD     "br make klibo@nblade.x86_64.debug.sim"


bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  switch (keycode) {
	  case MC0:
		  if (record->event.pressed) { SEND_STRING(TEST); } break;
	  case MC1:
		  if (record->event.pressed) { SEND_STRING(TEST);   } break;
	  case MC2:
		  if (record->event.pressed) { SEND_STRING(TEST);   } break;
	  case CIFS:
		  if (record->event.pressed) { SEND_STRING(CIFS_TRACE);   } break;
	  case ZSH:
		  if (record->event.pressed) { SEND_STRING(ZSHRC);        } break;
	  case BASH:
		  if (record->event.pressed) { SEND_STRING(BASHRC);       } break;
	  case MUL:
		  if (record->event.pressed) { SEND_STRING(MULTICHANNEL); } break;
	  case KLIBO:
		  if (record->event.pressed) { SEND_STRING(KLIBO_CMD);    } break;
	  case BRVSIM:
		  if (record->event.pressed) { SEND_STRING(BRVSIM_CMD);   } break;

  }
  return true;
};
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

	LAYOUT(
		KC_ESC  , KC_1    , KC_2    , KC_3    , KC_4    , KC_5    , KC_6    , KC_7    , KC_8    , KC_9    , KC_0    , KC_MINS , KC_EQL  , KC_BSLS , KC_GRV ,
		KC_TAB  , KC_Q    , KC_W    , KC_E    , KC_R    , KC_T    , KC_Y    , KC_U    , KC_I    , KC_O    , KC_P    , KC_LBRC , KC_RBRC , KC_BSPC ,
		KC_LCTL , KC_A    , KC_S    , KC_D    , KC_F    , KC_G    , KC_H    , KC_J    , KC_K    , KC_L    , KC_SCLN , KC_QUOT , KC_ENT  ,
		KC_LSFT , KC_Z    , KC_X    , KC_C    , KC_V    , KC_B    , KC_N    , KC_M    , KC_COMM , KC_DOT  , KC_SLSH , KC_RSFT , MO(1)   ,
		KC_NO   , KC_LGUI , KC_LALT , KC_SPC  , KC_DEL  , MO(2)   , KC_NO ) ,

	LAYOUT(
     	KC_ESC  , KC_F1   , KC_F2   , KC_F3   , KC_F4   , KC_F5   , KC_F6   , KC_F7   , KC_F8   , KC_F9   , KC_F10  , KC_F11  , KC_F12  , KC_INS  , KC_DEL ,
		RESET   , RGB_TOG , RGB_MOD , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_UP   , KC_TRNS , KC_TRNS ,
		KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_HOME , KC_PGUP , KC_LEFT , KC_RGHT , KC_TRNS ,
		KC_TRNS , KC_TRNS , KC_TRNS , BL_DEC  , BL_TOGG , BL_INC  , BL_STEP , KC_TRNS , KC_END ,  KC_PGDN , KC_DOWN , KC_TRNS , KC_TRNS ,
		KC_NO   , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_NO)  ,

	// MO(2): shell commands, maybe use it for tmux short cuts 
	LAYOUT(
		KC_TRNS , MC1     , MC2     , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , MC0 ,
		KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS ,
		KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KLIBO   , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS ,
		KC_TRNS , ZSH     , KC_TRNS , CIFS    , BRVSIM  , BASH    , KC_TRNS , MUL     , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS ,
		KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_TRNS , KC_NO)

};


void matrix_init_user(void) {
}

void matrix_scan_user(void) {
}

void led_set_user(uint8_t usb_led) {

	if (usb_led & (1 << USB_LED_NUM_LOCK)) {
		
	} else {
		
	}

	if (usb_led & (1 << USB_LED_CAPS_LOCK)) {
		DDRB |= (1 << 6); PORTB &= ~(1 << 6);
	} else {
		DDRB &= ~(1 << 6); PORTB &= ~(1 << 6);
	}

	if (usb_led & (1 << USB_LED_SCROLL_LOCK)) {
		
	} else {
		
	}

	if (usb_led & (1 << USB_LED_COMPOSE)) {
		
	} else {
		
	}

	if (usb_led & (1 << USB_LED_KANA)) {
		
	} else {
		
	}

}
